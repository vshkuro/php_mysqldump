<?php

//скрипт делает бекап таблиц используя библиотеку php 
//если нет возможности использовать mysqldump
//в массиве настроек $dumpSettings можно указать какие из таблиц дампить, или какие не бекапить
//в конце скрипт сжимает каждый дамп архиватором gzip и удаляет исходный дамп.
//каждая табличка в отдельном файле, 
//дампы лежат внутри директории dumps/

//как запускать (в фоне):
//nohup php php_redump.php &

define ("DBHOST", "127.0.0.1");
define ("DBNAME", "bd_name");
define ("DBUSER", "bd_user");
define ("DBPASS", "bd_pass");

include_once(dirname(__FILE__) . '/Mysqldump/Mysqldump.php');

if (!mysql_connect(DBHOST, DBUSER, DBPASS)) {
    echo 'Ошибка подключения к mysql';
    exit;
}

$sql = "SHOW TABLES FROM " . DBNAME;
$result = mysql_query($sql);

if (!$result) {
    echo "Ошибка базы, не удалось получить список таблиц\n";
    echo 'Ошибка MySQL: ' . mysql_error();
    exit;
}

$i = 0;
while ($row = mysql_fetch_row($result)) {
    $i++;
    echo "Таблица: {$row[0]}\n";

    $dumpSettings = array('include-tables' => array($row[0]),
                            'exclude-tables' => array('users_ref_stats',
                                                    'users_stats_ref_tovar',
                                                    'users_stats_ref_tovar_land',
                                                    'users_stats_ref_tovar_land_device'
                                                    ),
                            //'compress' => Mysqldump::NONE, /* CompressMethod::[GZIP, BZIP2, NONE] */
                            'no-data' => false,
                            'add-drop-table' => false,
                            'single-transaction' => true,
                            'lock-tables' => false,
                            'add-locks' => true,
                            'extended-insert' => true
                            );
    
    $dump = new Mysqldump\Mysqldump('mysql:host='.DBHOST.';dbname='.DBNAME.'', DBUSER, DBPASS, $dumpSettings);
    $dump->start('dumps/' . $row[0] . '.sql');
    unset($dump, $dumpSettings);

    // архивация файла через функцию exec()
    exec('gzip -9 dumps/' . $row[0] . '.sql');
    
    /*
    // если функция exec() отключена то можно попробовать данный способ
    $file = 'dumps/' . $row[0] . '.sql';

    // Name of the gz file we're creating
    $gzfile = 'dumps/' . $row[0] . '.gz';

    // Open the gz file (w9 is the highest compression)
    $fp = gzopen ($gzfile, 'w9');

    // Compress the file
    gzwrite ($fp, file_get_contents($file));

    // Close the gz file and we're done
    gzclose($fp);

    unlink('dumps/' . $row[0] . '.sql');
    */

}

mysql_free_result($result);


?>